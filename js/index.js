$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carosuel').carousel({
        interval: 100
    });
});

$('#contacto').on('show.bs.modal', function (e) {
    console.log('el modal se esta mostrando');

    $('#contactoBtn').removeClass('btn-outline-success');
    $('#contactoBtn').addClass('btn-primary');
    $('#contactoBtn').prop('disable', true);
});
$('#contacto').on('shown.bs.modal', function (e) {
    console.log('el modal se mostro');
});
$('#contacto').on('hide.bs.modal', function (e) {
    console.log('el modal se oculta');
});
$('#contacto').on('hidden.bs.modal', function (e) {
    console.log('el modal se ocultará');
    $('#contactoBtn').prop('disable', false);
});